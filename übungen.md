# Schrittweise Ersetzung

Um zu verstehen, was eine Zeile tut, kann man an der Zeile Schritt für Schritt
Ersetzungen vornehmen.

Dabei wird in jedem Schritt *exakt eines* der folgenden Dinge getan:

* Eine Variable wird durch ihren Inhalt ersetzt:

        2 + a
        2 + 4

        "hallo " + name
        "hallo " + "welt"

* Ein einzelner Operator und seine Operanden wird durch das Ergebnis der
  Operation ersetzt:

        1 + 2 + 3
        3 + 3

        1 == 2
        False

        "1" != 1
        True

        1 <= 2
        True

        "alpha" == "alpha"
        True

        "All Hail %s" % "Discordia"
        "All Hail Discordia"

        "ab" + "c"
        "abc"

        True == True
        True

        True and True
        True

        False and True
        False

        True or False
        True

* Klammern um einen einzelnen Wert können weggelassen werden:

        (a)
        a

        (-23)
        -23

        ("whatever")
        "whatever"


# Aufgabe 1

Mache solange eine *Schrittweise Ersetzung*, bis sich nichts mehr vereinfachen
lässt.

## Beispielaufgabe

Variablen: a = 1, b = "12", c = 4

        b == ((a + 2) * c)

## Lösung

        b == ((a + 2) * c)
        "12" == ((a + 2) * c)
        "12" == ((1 + 2) * c)
        "12" == ((3) * c)
        "12" == ((3) * 4)
        "12" == (3 * 4)
        "12" == (12)
        "12" == 12
        False

## Aufgabe 1.1

Variablen: amount = 100, tax = 0.18, item1_price = 23.55, item2_price = 17.23, budget = 1000

        (amount * (item1_price + item2_price) * tax) < budget

## Aufgabe 1.2

Variablen: name = "Eris"

        ("All hail %s" % name) + ", " + ("all hail %s" % ("Disc%s" % "ordia")) + '!'

## Aufgabe 1.3

        False or (((False and True) or (False or True)) and (True != False))

## Aufgabe 1.4

        ("alpha" == "Alpha" or 1 < 2) and True

## Aufgabe 1.5

Variablen: vorname = 'Alice', nachname = 'Arkham',

        (vorname == 'Alice' and nachname == 'Liddell') or (vorname == "Alice" and nachname == "Arkham")
